import sys
import time

import cv2
import imutils
import numpy as np

from imagezmq import imagezmq



class FrameServer:
    def __init__(self, client_ip, req_rep, port=5555):
        self.REQ_REP = req_rep
        self.port = port
        self.clients = [client_ip]
        self.hub = False

    def start(self):
        if not self.hub:
            client_ip = self.clients[0]
            self.hub = self.create_hub(client_ip)
        return self.hub

    def stop(self):
        self.hub = False
        return self.hub

    def create_hub(self, client='127.0.0.1'):
        if self.REQ_REP:
            client_ip = '*'
        else:
            client_ip = client

        connection = f'tcp://{client_ip}:{self.port}'
        print(connection)
        self.hub = imagezmq.ImageHub(open_port=connection, REQ_REP=self.REQ_REP)
        return self.hub

    def add_client(self, new_client_ip):
        self.clients.append(new_client_ip)

    def receive(self, index, buffer):
        if buffer:
            client_name, jpg_buffer = self.hub.recv_jpg()
            if self.REQ_REP:
                self.hub.send_reply(b'OK')

            frame = cv2.imdecode(np.frombuffer(jpg_buffer, dtype='uint8'), -1)
        else:
            client_name, frame = self.hub.recv_image()

        yield frame

    def show_frames(self, index=0, buffer=True):
        if self.hub:
            show = True
            while show:
                for frame in self.receive(index, buffer):
                    cv2.imshow('Frame', frame)

                    key = cv2.waitKey(1) & 0XFF
                    if key == ord('q'):
                        show = False
                        break

            cv2.destroyAllWindows()
        print('FrameServer has not started')
        return False



if __name__ == "__main__":
    import argparse # CLI - Arguments Parser

    ap = argparse.ArgumentParser()
    ap.add_argument("-c", "--client", required=False)
    ap.add_argument("-b", "--buffer", required=False)
    ap.add_argument("-r", "--reqrep", required=False)
    args = vars(ap.parse_args())

    TRUE = ['true', 'True', 'TRUE', 'y', 'Y', 'yes', 'Yes', 'YES']

    buff = args.get('buffer')
    if buff in TRUE:
        buff = True
    else:
        buff = False

    reqrep = args.get('reqrep')
    if reqrep in TRUE:
        reqrep = True
    else:
        reqrep = False

    print(args.get('client'))   

    server = FrameServer(client_ip=args.get('client'), req_rep=reqrep,)
    server.start()
    server.show_frames(buffer=buff)
