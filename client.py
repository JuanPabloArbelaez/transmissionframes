import socket
import time

import imutils
import cv2

from imutils.video import VideoStream
from imagezmq import imagezmq



class FrameClient:
    def __init__(self, req_rep, port=5555):
        self.REQ_REP = req_rep
        self.port = port
        self.client_name = socket.gethostname()
        self.sender = None
        self.stream = None

    def set_connection(self, port, server='127.0.0.1'):
        if self.REQ_REP:
            server_ip = server
        else:
            server_ip = '*'

        connection = f'tcp://{server_ip}:{self.port}'
        print(connection)
        
        self.sender = imagezmq.ImageSender(
            connect_to=connection, REQ_REP=self.REQ_REP)

        return self.sender

    def get_stream(self, source):
        self.stream = VideoStream(src=source).start()
        return self.stream

    def encode_img(self, img):
        _ , jpg_buffer = cv2.imencode(".jpg", img, [int(cv2.IMWRITE_JPEG_QUALITY), 95])
        return jpg_buffer

    def transmit(self, buffer, source=0, port=5555, delay=0.1):
        if not self.sender:
            self.set_connection(port)
        if not self.stream:
            self.get_stream(source)

        while True:
            time.sleep(delay)
            frame = self.stream.read()

            if buffer:
                jpg_buffer = self.encode_img(frame)
                self.sender.send_jpg(self.client_name, jpg_buffer)
            else:
                self.sender.send_image(self.client_name, frame)

    def transmit_ROI(self):
        pass

    def transmit_faces(self):
        pass



if __name__ == "__main__":
    import argparse # CLI - Arguments Parser

    ap = argparse.ArgumentParser()
    ap.add_argument("-s", "--source", required=False)
    ap.add_argument("-b", "--buffer", required=False)
    ap.add_argument("-r", "--reqrep", required=False)
    args = vars(ap.parse_args())

    TRUE = ['true', 'True', 'TRUE', 'y', 'Y', 'yes', 'Yes', 'YES']

    buff = args.get('buffer')
    if buff in TRUE:
        buff = True
    else:
        buff = False

    reqrep = args.get('reqrep')
    if reqrep in TRUE:
        reqrep = True
    else:
        reqrep = False

    client = FrameClient(req_rep=reqrep)
    client.transmit(
        #source = 0,
        #source = 'rtsp://admin:admin123$$@42.115.241.170:8778/Streaming/Channels/101',
        source = 'rtsp://syno:b14f29aa1130f924d71e468ab4883bc0@171.244.21.142:554/Sms=59.unicast',
        buffer = buff
    )
